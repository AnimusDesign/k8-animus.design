plugins {
    kotlin("jvm") version "1.3.70"
    application
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("io.fabric8:kubernetes-client:4.8.0")
    implementation("com.fkorotkov:kubernetes-dsl:2.7.1")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

application {
    mainClassName = "design.animus.k8.blog.DirectorKt"
}