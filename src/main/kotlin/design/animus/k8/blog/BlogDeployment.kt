package design.animus.k8.blog

import com.fkorotkov.kubernetes.*
import com.fkorotkov.kubernetes.apps.metadata
import com.fkorotkov.kubernetes.apps.selector
import com.fkorotkov.kubernetes.apps.spec
import com.fkorotkov.kubernetes.apps.template
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.Service
import io.fabric8.kubernetes.api.model.apps.Deployment

const val baseBlogName = "$applicationName-blog-web"

open class BlogService(private val serviceName: String = baseBlogName) : Service() {

    companion object {
        const val blogPort = 2368
        const val portName = "ghost-web-port"
    }

    init {
        metadata {
            name = serviceName
            labels = applicationLabels
            annotations = gitLabAnnotations + ingressAnnotations
        }
        spec {
            selector = applicationLabels
            ports = listOf(
                newServicePort {
                    port = blogPort
                    targetPort = IntOrString(blogPort)
                }
            )
            clusterIP = "None"
        }
    }
}


class BlogDeployment(serviceName: String = baseBlogName) : Deployment() {
    companion object BlogConfig {
        val volumeClaimName = "$baseBlogName-volume"

    }

    init {
        metadata {
            name = serviceName
            annotations = gitLabAnnotations + ingressAnnotations
            labels = applicationLabels
        }
        spec {
            replicas = 1
            selector {
                matchLabels = applicationLabels
            }
            template {
                metadata {
                    labels = applicationLabels
                }
                spec {
                    containers = listOf(
                        newContainer {
                            name = baseBlogName
                            image = ghostImage
                            imagePullPolicy = "Always"
                            ports = listOf(
                                newContainerPort {
                                    containerPort = BlogService.blogPort
                                }
                            )
                            volumeMounts = listOf(
                                newVolumeMount {
                                    name = BlogDeployment.volumeClaimName
                                    mountPath = "/var/lib/ghost/content"
                                    readOnly = false
                                }
                            )
                            env = listOf(
                                newEnvVar {
                                    name = "url"
                                    value = EnvConfig.envUrl
                                },
                                newEnvVar {
                                    name = "database__connection__host"
                                    value = dbBaseName
                                },
                                newEnvVar {
                                    name = "database__connection__user"
                                    value = "root"
                                },
                                newEnvVar {
                                    name = "database__connection__password"
                                    value = DBDeployment.password
                                },
                                newEnvVar {
                                    name = "database__connection__database"
                                    value = DBDeployment.database
                                }
                            )
                            volumes = listOf(
                                newVolume {
                                    name = volumeClaimName
                                    persistentVolumeClaim {
                                        claimName = volumeClaimName
                                    }
                                }
                            )
                        }
                    )
                }
            }
        }
    }
}

