package design.animus.k8.blog

import com.fkorotkov.kubernetes.extensions.*
import com.fkorotkov.kubernetes.metadata
import com.fkorotkov.kubernetes.resources
import design.animus.k8.blog.EnvConfig.ciEnvironmentSlug
import design.animus.k8.blog.EnvConfig.ciProjectPathSlug
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim
import io.fabric8.kubernetes.api.model.PersistentVolumeClaimSpec
import io.fabric8.kubernetes.api.model.Quantity
import io.fabric8.kubernetes.client.DefaultKubernetesClient

const val applicationName = "animus-design"
val applicationLabels = mapOf(
    "app" to applicationName
)
const val deployedURL = "blog.animus.design"
const val ghostImage = "ghost:latest"
const val dbImage = "bitnami/mariadb:10.3.22"
val nameSpace = System.getenv("KUBE_NAMESPACE") ?: "animus-design"

// GitLab Specific Elements
object EnvConfig {
    val ciProjectPathSlug = System.getenv("CI_PROJECT_PATH_SLUG") ?: ""
    val ciEnvironmentSlug = System.getenv("CI_ENVIRONMENT_SLUG") ?: ""
    val dbPassword = System.getenv("dbPassword") ?: "ghost"
    val dbUser = System.getenv("dbUser") ?: "ghost"
    val database = System.getenv("database") ?: "ghost"
    val envUrl = System.getenv("CI_ENVIRONMENT_URL") ?: "https://blog.animus.design"
    val hostName = envUrl
        .replace("https://", "")
        .replace("https://", "")
}

fun createPVCClaim(
    volumeClaimName: String,
    storageAmount: String,
    inLabels: Map<String, String>,
    inAccessMode: List<String> = listOf(
        "ReadWriteOnce"
    )
) = PersistentVolumeClaim().apply {
    metadata {
        name = volumeClaimName
        labels = inLabels
    }
    spec = PersistentVolumeClaimSpec().apply {
        accessModes = inAccessMode
        resources {
            requests = mapOf(
                "storage" to Quantity(storageAmount)
            )
        }
    }
}

fun main() {
    println("======================")
    println("Deploying Kotlin K8 DSL Ghost Instance")
    println("NameSpace: $nameSpace")
    println("CI Project Path Slug: $ciProjectPathSlug")
    println("CI Environment Path Slug: $ciEnvironmentSlug")
    val client = DefaultKubernetesClient()
    client.persistentVolumeClaims().inNamespace(nameSpace).createOrReplace(
        createPVCClaim(DBDeployment.volumeClaimName, "5Gi", applicationLabels)
    )
    client.persistentVolumeClaims().inNamespace(nameSpace).createOrReplace(
        createPVCClaim(BlogDeployment.volumeClaimName, "5Gi", applicationLabels)
    )
    println("Creating Database Service")
    client.services().inNamespace(nameSpace).createOrReplace(DBService())
    println("Creating Database Deployment")
    client.apps().deployments().inNamespace(nameSpace).createOrReplace(DBDeployment())
    println("Sleep 160s to allow for MariaDB to come up.")
    Thread.sleep(160000)
    println("Creating Blog Service")
    client.services().inNamespace(nameSpace).createOrReplace(BlogService())
    println("Creating Blog Deployment")
    client.apps().deployments().inNamespace(nameSpace).createOrReplace(BlogDeployment())
    println("Creating Ingress")
    client.inNamespace(nameSpace).extensions().ingresses().createOrReplace(
        newIngress {
            metadata {
                name = "$applicationName-ingress"
                annotations = gitLabAnnotations + ingressAnnotations

            }
            spec {
                tls = listOf(
                    newIngressTLS {
                        hosts = listOf(
                            EnvConfig.hostName
                        )
                        secretName = "$nameSpace-$applicationName-tls-secret"
                    }
                )
                rules = listOf(
                    newIngressRule {
                        host = EnvConfig.hostName
                        http = newHTTPIngressRuleValue {
                            paths = listOf(
                                newHTTPIngressPath {
                                    backend = newIngressBackend {
                                        serviceName = baseBlogName
                                        servicePort = IntOrString(BlogService.blogPort)
                                    }
                                }
                            )
                        }
                    }
                )
            }
        }
    )
}