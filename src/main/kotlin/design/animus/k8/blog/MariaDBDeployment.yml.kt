package design.animus.k8.blog

import com.fkorotkov.kubernetes.*
import com.fkorotkov.kubernetes.apps.*
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.Service
import io.fabric8.kubernetes.api.model.apps.Deployment

const val dbBaseName = "$applicationName-blog-db"

open class DBService(private val serviceName: String = dbBaseName) : Service() {


    companion object {
        const val mariaDBPort = 3306
        const val portName = "maria-db-port"
    }


    init {
        metadata {
            name = serviceName
            labels = applicationLabels
            annotations = gitLabAnnotations + ingressAnnotations
        }
        spec {
            selector = applicationLabels
            ports = listOf(
                newServicePort {
                    port = mariaDBPort
                    targetPort = IntOrString(mariaDBPort)
                }
            )
            clusterIP = "None"
        }
    }
}


class DBDeployment(serviceName: String = dbBaseName) : Deployment() {
    companion object DBConfig {
        val user = EnvConfig.dbUser
        val password = EnvConfig.dbPassword
        val database = EnvConfig.database
        const val volumeClaimName = "$dbBaseName-volume"
    }


    init {
        metadata {
            name = serviceName
            labels = applicationLabels
            annotations = gitLabAnnotations + ingressAnnotations
        }
        spec {
            replicas = 1
            selector {
                matchLabels = applicationLabels
            }
            strategy {
                type = "Recreate"
            }
            template {
                metadata {
                    labels = applicationLabels
                }
                spec {
                    containers = listOf(
                        newContainer {
                            name = dbBaseName
                            image = dbImage
                            imagePullPolicy = "Always"
                            ports = listOf(
                                newContainerPort {
                                    containerPort = DBService.mariaDBPort
                                    name = DBService.portName
                                }
                            )
                            volumeMounts = listOf(
                                newVolumeMount {
                                    name = volumeClaimName
                                    mountPath = "/bitnami/mariadb"
                                    readOnly = false
                                }
                            )
                            env = listOf(
                                newEnvVar {
                                    name = "MARIADB_USER"
                                    value = user
                                },
                                newEnvVar {
                                    name = "MARIADB_PASSWORD"
                                    value = password
                                },
                                newEnvVar {
                                    name = "MARIADB_DATABASE"
                                    value = database
                                },
                                newEnvVar {
                                    name = "MARIADB_ROOT_PASSWORD"
                                    value = password
                                }

                            )
                            volumes = listOf(
                                newVolume {
                                    name = volumeClaimName
                                    persistentVolumeClaim {
                                        claimName = volumeClaimName
                                    }
                                }
                            )
                        }
                    )
                }
            }
        }
    }
}

