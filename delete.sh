kubectl delete service -n $1 animus-design-blog-db
kubectl delete service -n $1 animus-design-blog-web
kubectl delete deployment -n $1 animus-design-blog-db
kubectl delete deployment -n $1 animus-design-blog-web
kubectl delete pvc -n $1 animus-design-blog-blog-volume
kubectl delete pvc -n $1 animus-design-blog-db-volume
kubectl delete namespace $1
kubectl create namespace $1

